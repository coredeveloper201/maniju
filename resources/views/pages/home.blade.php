<?php use App\Enumeration\Role; ?>
@extends('layouts.home_layout')
@section('additionalCSS')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }}">
<style type="text/css">
    .custom_padding_12_half {
    padding-left: 30px;
    padding-right: 30px;
}
p.large {
    font-size: 65px;
    letter-spacing: .375em;
    line-height: 1.4;
    font-weight: 300;
    padding-bottom: 1%;
}
 p.serif {
    font-family: "Cormorant Garamond", serif;
    color: #ffffff;
    text-transform: uppercase;
}
p.dark {
    color: #333333!important;
}
p.small {
    font-size: 14px;
    line-height: 2.5;
    font-weight: 500;
    letter-spacing: .5em;
}
p.sans {
    font-family: "Raleway", sans-serif;
    line-height: 1;
    color: #ffffff;
    margin: 0 auto;
    text-transform: uppercase;
}
.welcome_area .home-text-box-two {
    padding-bottom: 30px;
    padding-top: 25px;
}
</style>
@stop
@section('content')

<!-- =========================
    START BANNER SECTION
============================== -->
<section class="banner_area common_banner">
    <div class="  main-slider">
        <div class="row">
            <div class="col-md-12">
                @if(count($notification_banner_module) != 0)
                    @php
                        $imageUrl = asset($notification_banner_module[0]->image_path);
                    @endphp
                    @if($notification_banner_module[0]->status == 1)
                        @if($notification_banner_module[0]->image_path == '')
                            <div class="banner_top" style="color: white; background-color: black;background-image:url('')">
                            @if($notification_banner_module[0]->details == '')
                            <p></p>
                            @else
                            <?php echo $notification_banner_module[0]->details; ?>
                            @endif
                            </div>
                        @else
                            <div class="banner_top" style="background-image: url('{{asset($notification_banner_module[0]->image_path)}}')">
                            @if($notification_banner_module[0]->details == '')
                            <p></p>
                            @else
                            <?php echo $notification_banner_module[0]->details; ?>
                            @endif
                            </div>
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END BANNER SECTION
============================== -->
<!-- =========================
START BANNER SECTION
============================== -->
@if(isset($mainSliderImages) && $mainSliderImages[0])
<section class="banner_area clearfix main-slider">
    <div id="banner_slider" class="common_slide owl-carousel owl-theme">
        @foreach($mainSliderImages as $item)
            <div class="banner_inner">
                @if(strpos($item->image_path, '.mp4') !== false)
                    <video loop muted preload="metadata">
                        <source src="{{ asset($item->image_path) }}" type="video/mp4">
                    </video>
                @else
                    <a href="{{ $item->url }}">
                        <img class="img-fluid" src="{{ asset($item->image_path) }}" alt="No Image">
                    </a>
                @endif
            </div>
        @endforeach
    </div>
</section>
@endif

<!-- =========================
END BANNER SECTION
============================== -->

<!-- =========================
START WELCOME SECTION
============================== -->
@if (count($banners)>0)
<section class="welcome_area welcome_area-remove-top-padding">
    <div class="container home-page-95">
        <div class="text-center home-text-box">
            <p><a href="{{ url('/prom-2019') }}" class="home-text">Shop the Collection</a></p>
            <p><a href="{{ url('/prom-2019') }}" class="home-text">See All Gowns</a></p>
        </div>
        <div class="row">
            @foreach($banners as $ban)
            <div class="col-md-4 padding-min">
                <div class="welcome_inner">
                    <a href="{{ $ban->url }}">
                        <img class="img-fluid" src="{{ asset($ban->image_path) }}" alt="No Image">
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
<!-- =========================
END WELCOME SECTION
============================== -->

<!-- =========================
START WELCOME SECTION
============================== -->
@if (count($home_bannersTwo)>0)
<section class="welcome_area welcome_area-remove-top-padding">
    <div class="container">
        <div class="text-center home-text-box-two">
            <p><a class="home-text-two">ALL EYES<br>ON THE BRIDE<br><small>EVERYTHING YOU NEED TO SHINE</small></a></p>
        </div>
        <div class="row">
            @foreach($home_bannersTwo as $ban)
            <div class="col-md-6">
                <div class="welcome_inner">
                    <a href="{{ $ban->url }}">
                        <img class="img-fluid" src="{{ asset($ban->image_path) }}" alt="No Image">
                    </a>
                    <p class="head"><a href="{{ $ban->url }}">{{ $ban->head }}</a></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
<!-- =========================
END WELCOME SECTION
============================== -->

<!-- =========================
START HOME VIDEO SECTION
============================== -->
<section class="home_video_area">
    <div class="container home-page-100">
        <div class="row">
            <div class="col-md-12">
                <div class="home_video_inner">
                    {!! $page->content !!}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END HOME VIDEO SECTION
============================== -->

<!-- =========================
START HOME PRODUCT SLIDER SECTION
============================== -->
@if (count($newArrivalItems)>0)
<section class="product_slider_area">
    <div class="container home-page-95">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>New Arrivals</h2>
                <div class="home-page-slick">
                <div id="slick-banner1" class="common_slide owl-carousel owl-theme">
                    @foreach ( $newArrivalItems as $item )
                    <div class="product_slider_inner">
                        <a href="{{ route('product_single_page', $item->slug) }}">
                            @if ( isset($item->images[0]->image_path) )
                            <img src="{{ asset($item->images[0]->image_path) }}" alt="" class="img-fluid">
                            @else
                            <img src="{{ asset($item->images[0]->image_path) }}" alt="" class="img-fluid">
                            @endif
                        </a>
                    </div>
                    @endforeach
                </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="product_slide_link text-center">
                    <a href="{{ route('new_arrival_page') }}">View More</a>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- =========================
END HOME PRODUCT SLIDER SECTION
============================== -->

@if(count($section_three_images) != 0)
@php
$section_three_images = json_decode($section_three_images);
@endphp
<section class="welcome_area">
    <div class="container home-page-100" style="background: #f9f7f1;">
        <div class="text-center home-text-box-two">
            <p class="serif large dark">ON OUR MIND<span class="ls-fix">S</span></p>
            <p class="sans small dark desktop-only">What we’re loving, right now.</p>
        </div>
        <div class="row">
            @foreach($section_three_images as $image)
            <div class="col-md-6">
                <div class="welcome_inner">
                    <a href="{{ $image->url }}">
                        <img class="img-fluid" src="{{URL::to('')}}{{ $image->image_path }}" alt="No Image">
                    </a>
                    <p class="head"><a href="{{ $image->url }}">{{ $image->head }}</a></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif

@if(count($section_four_images) != 0)
@php
$section_three_images = json_decode($section_four_images);
@endphp
<section class="welcome_area">
    <div class="container home-page-100">
        <div class="row">
            @foreach($section_four_images as $image)
            <div class="col-md-12" style="padding: 0px;">
                <div class="welcome_inner">
                    <a href="{{ $image->url }}">
                        <img class="img-fluid" src="{{URL::to('')}}{{ $image->image_path }}" alt="No Image">
                    </a>
                    <div class="clearfix">&nbsp;</div>
                    <div class="clearfix">&nbsp;</div>
                    <p class="small-head"><a href="{{ $image->url }}">{{ $image->head }}</a></p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif


<!-- =========================
START HOME PRODUCT SLIDER SECTION
============================== -->

<section class="product_slider_area">
    <div class="container home-page-95">
        <div class="row">
            <div class="col-md-12">
                <h3>SHOP ALL BRIDESMAIDS</h3>
                <h2 class="color_pink">#Best Selling</h2>

                <div class="home-page-slick">
                    <div id="slick-banner2" class="common_slide owl-carousel owl-theme">
                        @foreach($maxOrderItems as $item)
                        <div class="product_slider_inner">
                            <a href="{{ route('product_single_page', $item->slug) }}">
                                <img src="{{ asset($item->images[0]->image_path) }}" alt="" class="img-fluid">
                            </a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="product_slide_link text-center">
                    <a href="{{ route('best_selling_page') }}">View More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
END HOME PRODUCT SLIDER SECTION
============================== -->
 <!-- =========================
        START HOME PRODUCT SLIDER SECTION
    ============================== -->

@if(count($social_feeds) > 0)
<section class="product_slider_area">
    <div class="container home-page-100">
        <div class="row">
            <div class="col-md-12">
                <h3>Show Us Instagram Post</h3>
                <h2 class="color_pink">#Instagram Post</h2>
                <div id="product_slider3" class="common_slide owl-carousel owl-theme">
                    @foreach($social_feeds as $feed)
                    <div class="product_slider_inner">
                        <a href="#">
                            <img src="{{ $feed->images->standard_resolution->url }}" alt="" class="img-fluid">
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif
<!-- =========================
        END HOME PRODUCT SLIDER SECTION
    ============================== -->

@if(count($footer_meta_settings) != 0)
<section class="product_slider_area two-block-text">
    <div class="container home-page-100">
        <div class="row">
            <div class="col-md-6 footer_section_style_1">
                <div align="center"><?php echo $footer_meta_settings[0]->meta_value; ?></div>
            </div>
            <div class="col-md-6 footer_section_style_2">
                <div align="center"><?php echo $footer_meta_settings[1]->meta_value; ?></div>
            </div>
        </div>
    </div>
</section>
@endif
@stop

@section('additionalJS')
<script src="{{ asset('plugins/slick/slick.js') }}" type="text/javascript" charset="utf-8"></script>
<script>
$(function() {
    $("#slick-banner1").slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 4,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                arrows: true,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 4
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: true,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
            }
        ]
    });

    $("#slick-banner2").slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 4,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                arrows: true,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 4
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: true,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
            }
            }
        ]
    });
});

$(function () {
var sliders = <?php echo json_encode($mainSliderImages)?>;

if (sliders[0].color == 'white')
    $('body').addClass('white_slide');

$('.slider').on('afterChange', function (event, slick, currentSlide) {
    if (sliders[currentSlide].color == 'white') {
        $('body').addClass('white_slide');
    } else {
        $('body').removeClass('white_slide');
    }
});
});
</script>
// Show Notification
@if ($welcome_msg!='')
<script>
setTimeout(function () {
    $('#modalWelcome').modal('show');
}, 100);
</script>
<div class="modal fade" id="modalWelcome">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">{!! $welcome_msg !!}</div>
        </div>
    </div>
</div>
@endif
@stop
