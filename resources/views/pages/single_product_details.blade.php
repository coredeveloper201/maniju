<?php
    use App\Enumeration\Role;
    use App\Enumeration\Availability;
?>

@extends('layouts.home_layout')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/magnifier/magnifier.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fotorama.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <style>
        .color-selected {
            border: 1px solid black !important;
        }
        .sub_category_menu
        {
            margin-left: 0px;
            padding-left: 5px;
            border-left: 1px solid #eee;
        }
        .text_header_txt
        {
            font-size: 14px;
            margin-bottom: 5px;
            font-weight: bold;
            color: #333;
        }
        .form-control {
            display: block;
            width: 100%;
            height: calc(2.25rem + -6px);
            padding: 3px;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
            transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        }

        .sharethis-inline-share-buttons {
            float: left;
        }

        .reviewDiv li {
            display:none;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5c9605599852eb0011ce9c5b&product=inline-share-buttons' async='async'></script>
@stop

@section('content')
    <!-- right side rating div -->
    <div class="full_screen_overlay"></div>
        <div v-if="loggedIn" class="review_rating_right_menu">
            <form action="{{ route('item_rating', $item['id']) }}" method="POST">
                {{ csrf_field() }}
                <h3>WRITE YOUR REVIEW</h3>
                <span class="close_ic mr-3">
                    <i class="fa fa-times-circle" style="font-size:28px"></i>
                </span>

                <div class="review_title clearfix">
                    @if(count($item['images']) > 0)
                        <img class="prod-image" src="{{ asset($item['images'][0]['image_path']) }}" alt="No image" />
                    @else
                        <img class="prod-image" src="{{ asset('/images/no-image.png') }}" />
                    @endif
                    <p>{{ $item['name'] }}</p>
                    {{-- <h2>currency+product.discount_price</h2> --}}
                </div>
                <div class="clearfix"></div>
                <div class="review_description clearfix">
                    <h2>Review Rating *</h2>
                    <div id="rateYo"></div>
                </div>
                <div class="form-group common_form">
                    <label>REVIEW TITLE</label>
                    <input type="text" class="form-control" name="title" required>
                </div>
                <div class="form-group common_form">
                    <label>WRITE REVIEW</label>
                    <textarea id="" cols="30" rows="10" class="form-control" name="comment" required></textarea>
                    <input type="hidden" id="rate_value" name="rate_value" value="">
                    <input type="hidden" name="product_id" value="{{ $item['id'] }}">
                </div>
                <p>Please avoid using any inappropriate language, personal information, HTML, references to other retailers or copyrighted comments.</p>
                <div class="form-group common_form">
                    <input type="submit" class="btn btn-primary btn-lg btn-block common-btn" value="SUBMIT">
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <!-- =========================
            START BREDCRUMS SECTION
        ============================== -->
        <section class="breadcrumbs_area">
            <div class="container container_full_width_mobile">
                <div class="row">
                    <div class="col-lg-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                <li class="breadcrumb-item active">@php echo $simgleProductDetails[0]->name; @endphp</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
        <!-- =========================
            START BREDCRUMS SECTION
        ============================== -->

        <!-- ===============================
            START PRODUCT THUMBNAIL SECTION
        =================================== -->
        <section class="product_single_area">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="single_product_slide">
                            <div id="single_product" class="owl-carousel owl-theme">
                                @foreach($simgleProductDetails[0]->images as $images)

                                    <div class="single_product_slide_inner">
                                        <a href="{{URL::to('/')}}/{{$images->image_path}}" class="magnifier-thumb-wrapper">
                                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                            <img src="{{URL::to('/')}}/{{$images->image_path}}" class="img-fluid thumb">
                                            @else
                                            <img src="{{$defaultItemImage_path}}" class="img-fluid thumb">
                                            @endif
                                        </a>
                                    </div>

                                @endforeach
                                @if ( $simgleProductDetails[0]->video != null )
                                <div class="single_product_slide_inner" style="background: #F8EDE9;">
                                    <a class="magnifier-thumb-wrapper">
                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <video controls src="{{ asset($simgleProductDetails[0]->video) }}" class="img-fluid thumb" style="height:100%;">
                                        @else
                                        <img src="{{$defaultItemImage_path}}" class="img-fluid thumb">
                                        @endif
                                    </a>
                                </div>
                                @endif
                            </div>
                            <div class='magnifier-preview mag-preview' id='preview'></div>

                            {{-- <ul class="single_product_social">
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest"></i></a></li>
                            </ul> --}}
                            <div class="sharethis-inline-share-buttons"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="single_product_description">
                            <h2>@php echo $simgleProductDetails[0]->name; @endphp</h2>
                            @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                            <p class="product_price">Price:
                            <span style="display:<?php echo $simgleProductDetails[0]->orig_price == '' ? 'none' : 'inline-block'?>; text-decoration: line-through;">$@php echo $simgleProductDetails[0]->orig_price; @endphp</span> $@php echo $simgleProductDetails[0]->price; @endphp</p>
                            @endif
                            <div class="product_rating clearfix">
                                <ul class="product_rating">
                                    <li>
                                        @if(count($review) > 0)
                                            {{-- <div class="customer_rateYo block" data-type="{{ $average_rating }}"></div><span>{{$average_rating}} ({{ count($review)  }})</span> --}}
                                            <div class="customer_rateYo block" data-type="{{ $average_rating }}"></div></span>
                                        @endif
                                    </li>
                                    {{-- <img src="images/translucent.gif" alt=""> --}}
                                    @guest
                                    <li><a class="no_modal" href="{{ route('buyer_login') }}">Write a review</a></li>
                                    @else
                                    <li><a class="right_modal" href="JavaScript:void(0);">Write a review</a></li>
                                    @endif
                                </ul>
                            </div>
                            <p class="text_header_txt">Style No: @php echo $simgleProductDetails[0]->style_no; @endphp</p>
                            <p class="text_header_txt">Fabric: @php echo $simgleProductDetails[0]->fabric; @endphp</p>
                            <div class="single_product_table">
                                @php
                                $pack_array = array();
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack1);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack2);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack3);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack4);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack5);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack6);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack7);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack8);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack9);
                                array_push($pack_array,$simgleProductDetails[0]->pack->pack10);
                                $pack_array = array_values(array_filter($pack_array));
                                @endphp
                                <table id='buy_info_table' class="table">
                                    <thead>
                                        <tr>
                                            <th width="35%">Color</th>
                                            <th style="text-align: center;">{{$simgleProductDetails[0]->pack->name}}</th>
                                            <th width="25%">Pack</th>
                                            <th>Qty</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($simgleProductDetails[0]->colors as $colors)
                                        <tr>
                                            <td>{{$colors->name}}</td>
                                            <td style="text-align: center;">
                                                @foreach($pack_array as $pack)
                                                    {{ $pack }}
                                                @endforeach
                                            </td>
                                            <td>
                                                {{--  <input type="text" class="form-control pack" data-color="{{ $colors->id }}" name="input-pack[]" onkeyup="change_pack_details(this)">  --}}
                                                <input type="text" class="form-control pack" data-color="{{ $colors->id }}" name="input-pack[]">
                                            </td>
                                            <td><span class="qty">0</span></td>
                                            <td>
                                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                                    <span class="price">$0.00</span>
                                                    <input type="hidden" class="input-price" value="0">
                                                @else
                                                    <span></span>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div

                            <p>
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                    <button class="add_cart_btn" id="btnAddToCart">add to cart</button>
                                    @if ($wishlistItem>0)
                                    <button class="add_cart_btn float-right" id="btnRemoveFromWishlist">remove from wishlist</button>
                                    @else
                                    <button class="add_cart_btn float-right" id="btnAddToWishlist">add to wishlist</button>
                                    @endif
                                @else
                                    <a href="{{ route('buyer_login') }}" class="add_cart_btn">Login to Add to Cart</a>
                                @endif
                            </p>
                            <div class="common_accordion single_product_accordion">
                                <div id="INFORMATION">
                                    <div class="card">
                                        <div class="card-header">
                                            <button class="btn-link collapsed" data-toggle="collapse" data-target="#INFO" >
                                                  Description
                                            </button>
                                        </div>
                                        <div id="INFO" class="collapse">
                                            <div class="card-body clearfix">
                                                <p>@php echo $simgleProductDetails[0]->description; @endphp</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1 offset-1 order-md-first no-padding">
                        <ul class="single_product_left_thumbnail">
                            @php
                            $image_count = 1;
                            @endphp
                            @foreach($simgleProductDetails[0]->images as $images)
                            <li>
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                    <img id="owlimage-{{$image_count}}" src="{{URL::to('/')}}/{{$images->image_path}}" class="img-fluid owlimage">
                                @else
                                    <img id="owlimage-{{$image_count}}" src="{{$defaultItemImage_path}}" class="img-fluid owlimage">
                                @endif
                            </li>
                            @php
                            $image_count++;
                            @endphp
                            @endforeach
                            @if ( $simgleProductDetails[0]->video != null )
                            <li>
                                @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                    <video id="owlimage-{{$image_count++}}" src="{{ asset($simgleProductDetails[0]->video) }}" class="owlimage" style="width:100%;">
                                @else
                                    <img id="owlimage-{{$image_count}}" src="{{$defaultItemImage_path}}" class="img-fluid owlimage">
                                @endif
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- ===============================
            END PRODUCT THUMBNAIL SECTION
        =================================== -->

        <!-- =========================
            START PRODUCT REVIEW SECTION
        ============================== -->
        <section class="product_review">
            <div class="container custom_container single_product_container">
                <div class="row">
                    <div class="col-md-12">
                        <h4>What people say about us</h4>
                        <div class="product_review_inner">
                            @if ( count($review) > 0 )
                            <ul class="reviewDiv">
                                @foreach ($review as $rk => $review_data)
                                <li>
                                    <div class="product_review_inner_img">
                                        <div class="image-wrapper">
                                            @if ( isset($review_data->avatar) )
                                            {{-- <img src="{{ asset(\App\Model\MetaBuyer::avatar_path().$review_data->avatar) }}" alt=""> --}}
                                            <img src="{{ asset('images/avatar/'.$review_data->avatar) }}" alt="">
                                            @else
                                            <img src="{{ asset('images/user.png') }}" alt="">
                                            @endif
                                        </div>
                                        <div class="customer_rateYo" data-type="{{ $review_data->rating }}"></div>
                                        <div class="rate-name-date-wrapper">
                                            <h5>{{ $review_data->first_name }} {{ $review_data->last_name }}</h5>
                                            <p>{{ date('d M Y', strtotime($review_data->updated_at)) }}</p>
                                        </div>
                                    </div>
                                    <blockquote class="review-title">{{ $review_data->title }}</blockquote>
                                    <h5>{{ $review_data->comment }}</h5>
                                </li>
                                @endforeach
                            </ul>
                                @if((count($review)>=4))
                                    <button class="add_cart_btn" id="loadreview">load more</button>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div class="related_item">
            <div class="container single_product_container">
                <div class="row">
                    <div class="col-md-12 custom_padding_5">
                        <div class="related_item_title">
                            <h2>You May Also Like</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <?php
                            function my_sort_function($a, $b)
                            {
                                return $a->orders < $b->orders;
                            }

                            if(count($similer_product_details) != 0)
                            {
                                usort($similer_product_details, 'my_sort_function');
                                $similler_product_records = array_slice($similer_product_details, 0, 5);
                            }
                        ?>
                        <div class="home-page-slick">
                            <div id="slick-banner1" class="common_slide owl-carousel owl-theme">
                                @foreach($similer_product_details as $similer_product_detail)
                                @if(sizeof($similer_product_detail->images) > 0)
                                <div class="product_slider_inner">
                                    <a href="{{ route('product_single_page', $similer_product_detail->slug) }}">
                                        @if (Auth::check() && Auth::user()->role == Role::$BUYER)
                                        <img src="{{URL::to('/')}}/{{$similer_product_detail->images[0]->image_path}}" alt="" class="img-fluid">
                                        @else
                                            <img src="{{$defaultItemImage_path}}" alt="" class="img-fluid">
                                        @endif
                                    </a>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/magnifier/Event.js') }}"></script>
    <script type="text/javascript" src="{{ asset('plugins/magnifier/Magnifier.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.js"></script>
    <script src="{{ asset('plugins/slick/slick.js') }}" type="text/javascript" charset="utf-8"></script>

    <script>
        $(function() {
            $("#slick-banner1").slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 5,
                responsive: [
                    {
                    breakpoint: 768,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 4
                    }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        arrows: true,
                        centerMode: true,
                        centerPadding: '40px',
                        slidesToShow: 2
                    }
                    }
                ]
            });
        });

        $(function () {
            $(".reviewDiv li").slice(0, 4).show();
            $("#loadreview").on('click', function (e) {
                e.preventDefault();
                $(".reviewDiv li:hidden").slice(0, 4).slideDown();
                if ($(".reviewDiv li:hidden").length == 0) {
                    $("#loadreview").fadeOut('slow');
                }
            });
        });

        $(document).ready(function(){
            // Magnifer
            var evt = new Event(),
            m = new Magnifier(evt);
            m.attach({
                thumb: '.thumb',
                largeWrapper: 'preview',
                zoom: 6
            });
            // Put preview id in active class
            // var innerPreviewDiv = "<div class='magnifier-preview' id='preview' style='width: 200px; height: 133px'>Starry Night Over The Rhone<br>by Vincent van Gogh</div>";
            // $(".owl-item.active .single_product_slide_inner").append(innerPreviewDiv);
        });

        $("#rateYo").rateYo({
            starWidth: "30px",
            rating: 0,
            fullStar: true,
            onChange: function (rating, rateYoInstance) {
                $('#rate_value').val(rating);
            }
        });

        $(".customer_rateYo").each(function(){
            var rating = $(this).attr('data-type');
            $(this).rateYo({
                rating: rating,
                starWidth: "15px",
                readOnly: true,
                normalFill: "#cfcfcf",
                ratedFill: "#000"
            });
        });

        $('.right_modal').click(function () {
            $('.review_rating_right_menu').addClass('review_rating_right_menu_open');
            $('.full_screen_overlay').addClass('overlay_open');
            return false;
        });

        $('.close_ic').click(function () {
            $('.review_rating_right_menu').removeClass('review_rating_right_menu_open');
            $('.full_screen_overlay').removeClass('overlay_open');
        });

        $(document).mouseup(function (e) {
            var container = $('.review_rating_right_menu');
            if (!container.is(e.target)
                && container.has(e.target).length === 0) {
                $('.review_rating_right_menu').removeClass('review_rating_right_menu_open');
                $('.full_screen_overlay').removeClass('overlay_open');
            }
        });

        var message = '{{ session('message') }}';
        if ( message != '' ) {
            toastr.success(message);
        }

        <?php
            echo "var pack_array = ".json_encode($pack_array)." ;";
        ?>

        var perPrice = parseFloat('{{ $simgleProductDetails[0]->price }}');
        var itemId = '{{ $simgleProductDetails[0]->id }}';
        var vendor_id = '';

        function change_thumbs_to_origin_image(obj_)
        {
            var original_image_path = $(obj_).data('original-image-path');
            $('.single_product_slide_inner').find('img').attr('src',original_image_path);
        }

        var itemInPack=0;
        for(var i in pack_array)
        {
            itemInPack += parseInt(pack_array[i]);
        }

        $('.pack').keyup(function () {
            var i = 0;
            var val = $(this).val();

            if (!isNaN(val) && val>0) {
                i = parseInt(val);

                if (i < 0)
                    i = 0;
            } else {
                $(this).val('');
            }

            $(this).closest('tr').find('.qty').html(itemInPack * i);
            $(this).closest('tr').find('.price').html('$' + (itemInPack * i * perPrice).toFixed(2));
            $(this).closest('tr').find('.input-price').val(itemInPack * i * perPrice);

            //calculate();

            $(this).focus();
        });

        {{--  function change_pack_details(obj)
        {
            if($(obj).val() == '')
            {
                $(obj).parent().parent().children('td').eq(3).html('0');
            }
            else
            {
                var input_val = parseInt($(obj).val());
                $(obj).parent().parent().children('td').eq(3).html(total * input_val);
            }
        }  --}}

        $('#btnAddToCart').click(function()
        {
            var colors = [];
            var qty = [];
            var total_qty = 0;
            var total_price = 0;

            $('.qty').each(function () {
                total_qty += parseInt($(this).html());
            });

            $('.input-price').each(function () {
                total_price += parseFloat($(this).val());
            });

            var valid = true;
            $('.pack').each(function () {
                var i = 0;
                var val = $(this).val();

                if (!isNaN(val) && val>0) {
                    i = parseInt(val);

                    if (i < 0)
                        return valid = false;
                } else {
                    if (val != '')
                        return valid = false;
                }

                if (i != 0) {
                    colors.push($(this).data('color'));
                    qty.push(i);
                }
            });

            if (total_qty>0 && total_price>0) {
                $.ajax({
                    method: "POST",
                    url: "{{ route('add_to_cart') }}",
                    data: { itemId: itemId, colors: colors, qty: qty, total_price: total_price, vendor_id: vendor_id },
                    headers: {
                        'X-CSRF-Token': '{!! csrf_token() !!}'
                    }
                }).done(function( data ) {
                    if (data.success) {
                        window.location.replace("{{ route('add_to_cart_success') }}");
                    } else {
                        $('#warningMsg').html(data.message);
                        $('#warningModal').modal('show');
                    }
                });

            } else {
                $('#warningMsg').html('Please select an item.');
                $('#warningModal').modal('show');
            }
        });

        // Wishlist
        $('#btnAddToWishlist').click(function () {
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('add_to_wishlist') }}",
                data: { id: itemId },
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            }).done(function( data ) {
                toastr.success('Added to Wishlist.');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        });

        $('#btnRemoveFromWishlist').click(function (e) {
            $this = $(this);

            $.ajax({
                method: "POST",
                url: "{{ route('remove_from_wishlist') }}",
                data: { id: itemId },
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            }).done(function( data ) {
                toastr.success('Removed from Wishlist.');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        });
    </script>
    <div class="modal fade" id="warningModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">System Alert</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body" id="warningMsg"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop
