<?php use App\Enumeration\OrderStatus; ?>
@extends('layouts.home_layout')

@section('content')

<!-- =========================
    START APPOINMENT SECTION
============================== -->
<section class="appoinment_area common_content_area">
    <div class="container">
        <div class="row">
            <div class="col-md-2 custom_padding_9 for_desktop d-none d-lg-block">
                <div class="common_left_menu">
                    @include('buyer.profile.menu')
                </div>
            </div>
            <div class="col-lg-10 col-md-12">
                <div class="row">
                    <div class="col-md-12 custom_padding_9">
                        <div class="my_account_area">

                            <div class="myaccount_title">
                                <h2>My Account</h2>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                            {{--  <div class="my_dashboard_title text-center">
                                <h2>Hello, {{$buyerInfo->first_name.' '.$buyerInfo->last_name}}</h2>
                                <p>Welcome to Maniju Fashion</p>
                            </div>  --}}
                            <div class="row">
                                <div class="col-md-6 my_dasboard_custom_padding">
                                    <h4 class="border-bottom">MY PROFILE</h4>
                                    <div class="my_dashboard_inner my_info_bg_4 buyer-dashboard-box">
                                        <p>The fundamental and any vital stats you wish to share about yourself and your event.</p>
                                        <a href="{{route('buyer_my_information')}}">Edit/View in your profile information &nRightarrow;</a>
                                    </div>
                                </div>

                                <div class="col-md-6 my_dasboard_custom_padding">
                                    <h4 class="border-bottom">CREDIT CARD & BILLING</h4>
                                    <div class="my_dashboard_inner my_info_bg_4 buyer-dashboard-box">
                                        <p>Secure storage and management for your payment and billing preferences.</p>
                                        <a href="{{route('buyer_billing')}}">Edit/View in your profile information &nRightarrow;</a>
                                    </div>
                                </div>

                                <div class="col-md-6 my_dasboard_custom_padding">
                                    <h4 class="border-bottom">MY WISHLIST</h4>
                                    <div class="my_dashboard_inner my_info_bg_4 buyer-dashboard-box">
                                        <p>Your wishlist with us. Revisit your wishlist & make an order.</p>
                                        <a href="{{route('view_wishlist')}}">Edit/View in your profile information &nRightarrow;</a>
                                    </div>
                                </div>

                                <div class="col-md-6 my_dasboard_custom_padding">
                                    <h4 class="border-bottom">ORDER HISTORY</h4>
                                    <div class="my_dashboard_inner my_info_bg_4 buyer-dashboard-box">
                                        <p>Your history with us. Revisit past purchase and shipped order information.</p>
                                        <a href="{{route('buyer_show_orders')}}">Edit/View in your profile information &nRightarrow;</a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END APPOINMENT SECTION
============================== -->
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            $('#btnApprove').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 2 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });

            $('#btnDecline').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 1 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });
        });


        @if ($welcome_msg!='')
        setTimeout(function () {
            $('#modalWelcome').modal('show');
        }, 100);
        </script>
        <div class="modal fade" id="modalWelcome">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">{!! $welcome_msg !!}</div>
                </div>
            </div>
        </div>
        @endif
@stop
