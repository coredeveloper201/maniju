<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('themes/front/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/front/css/owl.theme.default.css') }}">
    <link rel="stylesheet" href="{{ asset('themes/front/css/main.css') }}?id={{ rand() }}">
    <link rel="stylesheet" href="{{ asset('themes/front/css/custom.css') }}?id={{ rand() }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">

</head>
<body class="login_page">
    <!-- Header -->
    @include('layouts.shared.header')
    <!-- Header -->

    <!-- Content -->
    <div class="container">
        <section class="login_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="login_heading">
                            <h2>SIGN IN</h2>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="login_inner match_item">
                            <h4>I am already a registered {{$_ENV['SITE_NAME']}} user</h4>
                            <p>Enter your email address and password to log in.</p>
                            <form class="login-box" method="post" action="{{ route('buyer_login_post') }}">
                                @csrf
                                <div class="login_inner_form">
                                    <div class="form-group l_f_input">
                                        <input type="email" name="email" id="email" class="form-control" required>
                                        <label class="form-control-placeholder" for="name">Email</label>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="form-control" required>
                                        <label class="form-control-placeholder" for="password">Password</label>
                                    </div>

                                    <div class="custom_checkbox">
                                        <input type="checkbox" id="remember_me" name="remember_me" checked>
                                        <label for="remember_me" style="font-size: 14px">Remember me</label>
                                    </div>

                                    <p class="tex alert-info">{{ session('message') }}</p>
                                    <a href="{{ route('password_reset_buyer') }}">Forgot your password?</a>
                                    <p>
                                        <button class="cq_btn medium">LOG IN</button>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="login_inner match_item">
                            <div class="login_inner_form">
                                <h2>I want a {{$_ENV['SITE_NAME']}} user account</h2>
                                <p>If you still don't have a <b>{{$_ENV['SITE_URL_NAME']}}</b> account, use this option to access the registration form.</p>
                                <p>Provide your details to make <b>{{$_ENV['SITE_URL_NAME']}}</b> purchases easier.</p>
                                <button type="button" class="cq_btn medium" onclick="window.location.href='{{ route('buyer_register') }}'">Create Account</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Content -->

    <!-- Footer -->
    @include('layouts.shared.footer')
    <!-- Footer -->
<script src="{{ asset('themes/front/js/vendor/jquery-3.3.1.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
    crossorigin="anonymous"></script>
<script src="{{ asset('themes/front/js/vendor/bootstrap.js') }}"></script>
<script src="{{ asset('themes/front/js/owl.carousel.js') }}"></script>
<script src="{{ asset('themes/front/js/jquery.matchHeight.js') }}"></script>
<script src="{{ asset('themes/front/js/main.js') }}?id={{ rand() }}"></script>
</body>
</html>
