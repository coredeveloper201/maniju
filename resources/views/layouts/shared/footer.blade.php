<!-- =========================
    START FOOTER SECTION
============================== -->
<style>
    .footer_bg_dynamic{
        background-color: #{{ isset($footer_bg_color) ? $footer_bg_color : '' }}
    }
    .footer_font_dynamic .footer_bottom_list li,
    .footer_font_dynamic .footer_bottom_list li a{
        color: #{{ isset($footer_font_color) ? $footer_font_color : '' }}
    }
</style>
<div class="footer_false"></div>
<footer class="footer_area ">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footer_top_menu clearfix">
                    <ul>
                        <li><a href="{{ route('about_us') }}">About Us</a></li>
                        <li><a href="{{ route('contact_us') }}">Contact Us</a></li>
                        <li><a href="{{ route('size_guide') }}">Size Guide</a></li>
                    </ul>
                </div>

                <p id="mail_chimp_message" class="col-sm-6 offset-sm-3 alert"></p>
                <div class="footer_inner_wrapper clearfix">
                    {{-- <div class="footer_inner footer_inner_blog">
                        <a href="#">MANIJU Blog</a>
                    </div> --}}
                    <div class="footer_inner">
                        <ul class="footer_social">
                            @if ( isset($social_links->facebook) && $social_links->facebook != '' )
                            <li><a href="{{ $social_links->facebook }}"><i class="fab fa-facebook-f"></i></a></li>
                            @endif
                            @if ( isset($social_links->twitter) && $social_links->twitter != '' )
                            <li><a href="{{ $social_links->twitter }}"><i class="fab fa-twitter"></i></a></li>
                            @endif
                            @if ( isset($social_links->pinterest) && $social_links->pinterest != '' )
                            <li><a href="{{ $social_links->pinterest }}"><i class="fab fa-pinterest-p"></i></a></li>
                            @endif
                            @if ( isset($social_links->instagram) && $social_links->instagram != '' )
                            <li><a href="{{ $social_links->instagram }}"><i class="fab fa-instagram"></i></a></li>
                            @endif
                            @if ( isset($social_links->google_plus) && $social_links->google_plus != '' )
                            <li><a href="{{ $social_links->google_plus }}"><i class="fab fa-google"></i></a></li>
                            @endif
                            @if ( isset($social_links->whatsapp) && $social_links->whatsapp != '' )
                            <li><a href="{{ $social_links->whatsapp }}"><i class="fab fa-whatsapp"></i></a></li>
                            @endif
                        </ul>
                    </div>

                    <div class="footer_inner footer_sign_up">
                        <input type="email" name="mail_to_add" id="mail_to_add" class="form-control" required placeholder="sign up to receive our emails">
                        <button type="submit" id="mailchimp_add">JOIN</button>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-12">
                <div class="footer_long_desc">
                    <h2>About MANIJU</h2>
                    <p>Hi there! Thanks for stopping by. BHLDN (pronounced beholden) is your one-stop shop for all things bridal and event. Brought to you exclusively by Anthropologie, BHLDN Weddings offers a full assortment of wedding dresses, bridesmaid dresses, accessories, and décor for price-conscious brides that still want to WOW. We're here for every aspect of your big day, <a href="#">wedding dresses,</a> <a href="#">bridesmaid dresses,</a> <a href="#">accessories,</a> <a href="#">mother of the bride dresses,</a> <a href="#">wedding gowns, and even special events.</a> With styles from designers <a href="#">Badgley Mischka,</a> <a href="#">Catherine Deane,</a> <a href="#">Donna Morgan,</a> <a href="#">Jenny Yoo,</a> <a href="#">Monique Lhuillier,</a> <a href="#">Needle & Thread,</a> <a href="#">Tadashi Shoji,</a> Whispers & Echoes, Yumi Kim and more, you are sure to find your perfect gown and accessories. Some of our gown styles are a-line, modern ballgowns, lace dresses, and more. Our web stylists are available to answer any styling questions you have, from which delicate earring to wear to whether that bridal sash goes with your wedding dress. Not sure where to start with your bridal party? Check out our introduction to mix & match bridesmaids dresses which will give your BHLDN bridesmaids a chance to shine, and most importantly, feel comfortable. If you are just looking for a <a href="#">wedding guest dress</a> we have a curated assortment of occasion dresses that will make eyes turn at the ceremony. Don't forget to check out our new arrivals, little white dresses, bridal accessories, bridesmaid robes , and bridal separates before you leave! We love our #BHLDNbrides and can't wait to help you celebrate with our BHLDN <a href="#">wedding dresses</a>. Head over to our <a href="#">wedding blog</a> for more inspiration, tips AND advice for all your wedding or event needs.</p>
                </div>
            </div> --}}
        </div>
    </div>

    <div class="footer_bottom mt-2 {{ isset($footer_bg_color) ? 'footer_bg_dynamic' : '' }} {{ isset($footer_font_color) ? 'footer_font_dynamic' : '' }}">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="footer_bottom_list">
                        <li>© MANIJU {{ date('Y') }} </li>
                        <li><a href="{{ route('check_orders') }}">CHECK ORDERS</a></li>
                        <li><a href="{{ route('return_policy') }}">RETURN POLICY</a></li>
                        <li><a href="{{ route('payment_shipping') }}">PAYMENT/SHIPPING</a></li>
                        <li><a href="{{ route('terms_and_conditions') }}">TERMS AND CONDITIONS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- =========================
    END FOOTER SECTION
============================== -->
