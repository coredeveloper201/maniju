<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $meta_title }}</title>
    <meta name="description" content="{{ $meta_description }}" />

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel='stylesheet' href='{{ asset('themes/cq/fonts/stylesheet.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/owl.theme.default.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/owl.carousel.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/slick.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/magnific-popup.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/main.css') }}?id={{ rand() }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/custom.css') }}'/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    @yield('additionalCSS')
</head>

<body class="product_page my_account_page">
    <!-- Header -->
    @include('layouts.shared.header')
    <!-- Header -->

    <!-- Left Menu -->
    <div class="header_menu_item">
        <div class="header_menu_item_inner">
            <div class="menu_info">
                <p>
                    <a class="btn menu_info_head" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        MENU +
                    </a>
                </p>
                <div class="collapse" id="collapseExample">
                    <div class="main_cat">
                        <ul>
                            <li><a href="{{ route('new_arrival_page') }}">NEW ARRIVALS</a></li>
                            @foreach($default_categories as $cat)
                                <li><a href="{{ route('category_page', ['category' => changeSpecialChar($cat['name'])]) }}">{{ $cat['name'] }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="sub_cat myaccont_menu">
                <ul>
                    <li><a href="{{ route('buyer_show_overview') }}">MY ACCOUNT</a></li>
                    <li><a href="{{ route('buyer_show_orders') }}">ORDERS</a></li>
                    {{--<li><a href="{{ route('view_wishlist') }}">WISHLIST</a></li>--}}
                    <li><a href="{{ route('buyer_show_profile') }}">PROFILE </a></li>
                    <li><a href="{{ route('buyer_show_address') }}">ADDRESS BOOK </a></li>
                    <li><a href="#" class="btnLogOut">Log out </a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Left Menu -->

    <!-- Content -->
    <section class="my_account_area">
        @yield('content')
    </section>
    <!-- Content -->

    <!-- Footer -->
    @include('layouts.shared.footer')
    <!-- Footer -->

    <form id="logoutForm" class="" action="{{ route('logout_buyer') }}" method="post">
        {{ csrf_field() }}
    </form>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src='{{ asset('themes/cq/js/owl.carousel.js') }}'></script>
    <script src='{{ asset('themes/cq/js/jquery.magnific-popup.js') }}'></script>
    <script src='{{ asset('themes/cq/js/slick.js') }}'></script>
    <script src='{{ asset('themes/cq/js/main.js') }}?id={{ rand() }}'></script>
    <script>
        $(function () {
            $('.btnLogOut').click(function () {
                $('#logoutForm').submit();
            });
        });
    </script>
    @yield('additionalJS')
</body>
</html>