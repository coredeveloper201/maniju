<div class="form-group row">
    <div class="col-lg-1">
        <label for="company_description" class="col-form-label">User ID</label>
    </div>

    <div class="col-lg-4">
        <input type="text" class="form-control" value="{{ $user->user_id }}" disabled>
    </div>
</div>

<div class="form-group row">
    <div class="col-lg-1">
        <label for="company_description" class="col-form-label">First & Last Name</label>
    </div>

    <div class="col-lg-2">
        <input type="text" class="form-control" id="adminFirstName" value="{{ $user->first_name }}">
    </div>

    <div class="col-lg-2">
        <input type="text" class="form-control" id="adminLastName" value="{{ $user->last_name }}">
    </div>
</div>

<div class="form-group row">
    <div class="col-lg-1">
        <label for="company_description" class="col-form-label">Password</label>
    </div>

    <div class="col-lg-4">
        <input type="password" class="form-control" id="adminPassword">
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-right">
        <button class="btn btn-primary" id="adminBtnSave">Save</button>
    </div>
</div>