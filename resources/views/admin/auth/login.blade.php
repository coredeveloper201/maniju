<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Froya') }}</title>


    <link rel="stylesheet" media="screen" href="{{ asset('themes/front_old/css/vendor.min.css') }}">
    <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('themes/front_old/css/styles.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin-login.css') }}">
</head>

<!-- Body-->
<body>
<!-- Off-Canvas Wrapper-->
<div class="container">
    <div class="sign_in_view">
        <img id="profile-img" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" class="profile-img-card">
        <p id="profile-name" class="profile-name-card"></p>
        <form method="post" action="{{ route('login_admin_post') }}">
            @csrf

            <div class="form-group">
                <input class="form-control" type="text" placeholder="Username" name="username" value="{{ old('username') }}" required>
            </div>
            <div class="form-group">
                <input class="form-control" type="password" placeholder="Password" name="password" required>
            </div>
            <div class="d-flex flex-wrap justify-content-between">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="remember_me" name="remember_me" checked>
                    <label class="custom-control-label" for="remember_me">Remember me</label>
                </div>
            </div>

            <div class="has-danger">
                <div class="form-control-feedback">{{ session('message') }}</div>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Login</button>
        </form>
    </div>
</div>

</body>
</html>