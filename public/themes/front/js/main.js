$(document).ready(function(){


    (function() {
        $(window).on('scroll', function() {
            /*
            =========================================================================================
            2. NAVBAR
            =========================================================================================
            */

            // if ($(window).scrollTop() > 28) {
            //     $(".header_area").addClass("fixed-top");
            // } else {
            //     $(".header_area").removeClass("fixed-top");
            // }


        });
    }());


        $(".showhide").click(function(){
            $(".mobile_menu").addClass('menu_open');
            return false;
        });

        $('.showhide').click(function(){
            $('.mobile_menu').addClass('menu_open');
            $('.mobile_overlay').addClass('mobile_overlay_open');
            $('.ic_c_mb').addClass('ic_c_mb_show');
            return false;
        });
        $('.ic_c_mb').click(function(){
            $('.mobile_menu').removeClass('menu_open');
            $('.mobile_overlay').removeClass('mobile_overlay_open');
            $('.ic_c_mb').removeClass('ic_c_mb_show');
        });
        $('.mobile_overlay').click(function(){
            $('.mobile_menu').removeClass('menu_open');
            $('.mobile_overlay').removeClass('mobile_overlay_open');
            $('.ic_c_mb').removeClass('ic_c_mb_show');
        });

        $(".header_right_mobile ul li i").click(function(){
            $(".header_search_mobile").toggle();
            return false;
        });

        $(document).mouseup(function (e){
            var container = $('.header_search_mobile');
            if (!container.is(e.target)
              && container.has(e.target).length === 0)
            {
              $('.header_search_mobile').hide();
            }
        });

        /*
        =========================================================================================
         PRODUCT 1 SLIER
        =========================================================================================
        */

       var banner_slider = jQuery("#banner_slider");
       banner_slider.owlCarousel({
           loop: true,
           margin: 20,
           lazyLoad:true,
           smartSpeed: 1500,
           autoplay:true,
           nav: true,
           dots:true,
           navText: ["<img src='themes/front/images/product/left-arrow.png'>","<img src='themes/front/images/product/right-arrow.png'>"],
           responsive: {
               0: {
                   items: 1
               },
               400: {
                   items: 1
               },
               768: {
                   items: 1
               },
               1200: {
                   items: 1
               }
           }
       });

       /*
        =========================================================================================
         PRODUCT 1 SLIER
        =========================================================================================
        */


        /*
        =========================================================================================
         PRODUCT 1 SLIER
        =========================================================================================
        */

        var product_slider1 = jQuery("#product_slider1");
        product_slider1.owlCarousel({
            center: true,
            loop: true,
            margin: 20,
            lazyLoad:true,
            smartSpeed: 1500,
            autoplay:true,
            nav: true,
            dots:true,
            navText: ["<img src='themes/front/images/product/left-arrow.png'>","<img src='themes/front/images/product/right-arrow.png'>"],
            responsive: {
                0: {
                    items: 2
                },
                400: {
                    items: 2
                },
                768: {
                    items: 4
                },
                1200: {
                    items: 4
                }
            }
        });

        /*
        =========================================================================================
            PRODUCT 1 SLIER
        =========================================================================================
        */

        var product_slider2 = jQuery("#product_slider2");
        product_slider2.owlCarousel({
            center: true,
            autoWidth: true,
            loop: true,
            margin: 5,
            lazyLoad:true,
            smartSpeed: 1500,
            autoplay:true,
            nav: true,
            dots:true,
            navText: ["<img src='themes/front/images/product/left-arrow.png'>","<img src='themes/front/images/product/right-arrow.png'>"],
            responsive: {
                0: {
                    items: 2
                },
                400: {
                    items: 2
                },
                768: {
                    items: 4
                },
                1200: {
                    items: 5
                }
            }
        });

         var product_slider2 = jQuery("#product_slider3");
        product_slider2.owlCarousel({
            loop: true,
            margin: 20,
            lazyLoad:true,
            smartSpeed: 1500,
            autoplay:true,
            nav: true,
            dots:true,
            navText: ["<img src='themes/front/images/product/left-arrow.png'>","<img src='themes/front/images/product/right-arrow.png'>"],
            responsive: {
                0: {
                    items: 2
                },
                400: {
                    items: 2
                },
                768: {
                    items: 2
                },
                1200: {
                    items: 4
                }
            }
        });

        /*
        =========================================================================================
            PRODUCT 1 SLIER
        =========================================================================================
        */

        var single_product = jQuery("#single_product");
        single_product.owlCarousel({
            loop: false,
            margin: 0,
            lazyLoad:true,
            smartSpeed: 500,
            autoplay:false,
            nav: false,
            dots: true,
            responsive: {
                0: {
                    items: 1
                },
                400: {
                    items: 1
                },
                768: {
                    items: 1
                },
                1200: {
                    items: 1
                }
            }
        });

        $('.owlimage').click(function (e) {
            e.preventDefault();
            var order = $(this).attr('id').split('-')[1];
            $('.owl-dot:nth-child('+order+')').trigger('click');
        });

        /*
        =========================================================================================
            PRODUCT 1 SLIER
        =========================================================================================
        */

        var single_like_product = jQuery("#single_like_product");
        single_like_product.owlCarousel({
            center: true,
            loop: true,
            margin: 10,
            lazyLoad:true,
            smartSpeed: 1500,
            autoplay:true,
            dots:true,
            nav: true,
            navText: ["<img src='themes/front/images/product/left-arrow.png'>","<img src='themes/front/images/product/right-arrow.png'>"],
            responsive: {
                0: {
                    items: 3
                },
                400: {
                    items: 3
                },
                768: {
                    items: 5
                },
                1200: {
                    items: 6
                }
            }
        });

        /*
        =========================================================================================
            PRODUCT 1 SLIER
        =========================================================================================
        */

        /*
        =========================================================================================
            MATCH HEIGHT
        =========================================================================================
        */
        $('.match_item').matchHeight({
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        });

});

