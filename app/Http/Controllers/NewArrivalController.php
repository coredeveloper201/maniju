<?php

namespace App\Http\Controllers;

use App\Enumeration\PageEnumeration;
use App\Enumeration\Role;
use App\Model\Category;
use App\Model\Item;
use App\Model\MasterColor;
use App\Model\MetaVendor;
use App\Model\TopBanner;
use App\Model\WishListItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NewArrivalController extends Controller
{
    public function showItems()
    {
        $date = '';
        $cat = '';

        $query = Item::query();
        if ( Auth::guest() ) {
            $query->where('status', 1)->where('guest_image', 1);
        }
        else {
            $query->where('status', 1)->where('guest_image', 0);
        }

        if (isset($request->D)) {
            if ($request->D != 'A') {
                $date = date('Y-m-d', strtotime("-$request->D day"));
                $query->whereDate('created_at', $date);
            }
        }

        if (isset($request->C)) {
            $cat = $request->C;
            $query->where('default_parent_category', $request->C);
        }

        $query->orderBy('created_at', 'desc');
        //$items = $query->paginate(30);
        $items = [];

        // Category
        $categories = Category::where('parent', 0)
            ->orderBy('sort')
            ->get();

        foreach ($categories as &$category) {
            $categoryCountQuery = Item::query();
            $categoryCountQuery->where('status', 1)->where('default_parent_category', $category->id);

            if ($date == '') {
                $category->count = $categoryCountQuery->count();
            } else {
                $category->count = $categoryCountQuery->whereDate('created_at', $date)->count();
            }
        }

        $default_categories = $categories;

        // Arrival Dates
        $totalCount = Item::where('status', 1)->count();
        $byArrivalDate = [];
        $byArrivalDate[] = [
            'name' => 'All',
            'count' => number_format($totalCount),
            'day' => 'A'
        ];

        for($i=0; $i <= 6; $i++) {
            $count = Item::where('status', 1)
                ->whereDate('created_at', date('Y-m-d', strtotime("-$i day")))
                ->count();

            $byArrivalDate[] = [
                'name' => date('F j', strtotime("-$i day")),
                'count' => number_format($count),
                'day' => $i
            ];
        }

        // Vendors
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')->get();

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        // Master Colors
        $masterColors = MasterColor::orderBy('name')->get();

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        // New Arrivals
        $today = Carbon::today();
        $newArrivalItems = Item::where('status', 1)->orderBy('created_at', 'desc')->with('images')->where('created_at', '>', $today->subDays(7))->get();


         /*Notification Banner module*/
        $top_notification_banner_module = DB::table('top_banners')->where('page', '10')->get();

        if(count($top_notification_banner_module) == 0)
        {
            $top_notification_banner_module = [];
        }

        return view('pages.new_arrival', compact('items', 'newArrivalItems', 'byArrivalDate', 'categories', 'default_categories', 'vendors',
            'masterColors', 'wishListItems', 'defaultItemImage_path','top_notification_banner_module'));
    }

    public function get_new_arrival_items_load_ajax()
    {
        $today = Carbon::today();
        // New Arrivals
        $query = Item::where('status', 1)->with('images.color')->where('created_at', '>', $today->subDays(60));

        if ( isset($_GET['sort_by']) && $_GET['sort_by'] == 'low_to_high' ) {
            $query->orderBy('price', 'asc');
        }
        if ( isset($_GET['sort_by']) && $_GET['sort_by'] == 'high_to_low' ) {
            $query->orderBy('price', 'desc');
        }

        $query->orderBy('created_at', 'desc');

        $total_record = $query->get();

        $offset = (int) $_GET['offset'];
        $limit = $_GET['limit'];
        $skip = ($offset - 1) * $limit;

        $query->skip($skip)->take($limit);
        $query = $query->get();
        $last_pagination_index = ceil(count($total_record) / $limit);

        $data = array();
        $data['total_record'] = count($total_record);
        $data['records'] = $query;
        $data['offset'] = $offset;

        $data['last_pagination_index'] = $last_pagination_index;

        return json_encode($data);
    }
}
