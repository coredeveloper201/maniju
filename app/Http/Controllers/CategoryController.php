<?php

namespace App\Http\Controllers;

use App\Enumeration\Role;
use App\Enumeration\VendorImageType;
use App\Model\BodySize;
use App\Model\Category;
use App\Model\Item;
use App\Model\ItemCategory;
use App\Model\ItemView;
use App\Model\MasterColor;
use App\Model\MasterFabric;
use App\Model\MetaVendor;
use App\Model\Pattern;
use App\Model\Setting;
use App\Model\Style;
use App\Model\VendorImage;
use App\Model\Visitor;
use App\Model\WishListItem;
use App\Model\Page;
use App\Enumeration\PageEnumeration;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

use DrewM\MailChimp\MailChimp;
use Spatie\Newsletter\Newsletter;
use Spatie\Newsletter\NewsletterListCollection;
use Vinkla\Instagram\Instagram;

class CategoryController extends Controller
{
    public function Category(Request $request, $slug)
    {
        $category = Category::where('slug', '=', $slug)->first();
        if (!empty($category)) {
            $query = Item::where('status', 1)->with('images.color')->where(function($query) use($category) {
                $query->where('default_parent_category', $category->id)
                ->orWhere('default_second_category', $category->id)
                ->orWhere('default_third_category', $category->id);
            });

            if (isset($request->sort_by)) {
                if ($request->sort_by=='low_to_high') {
                    $query->orderBy('price', 'asc');
                } elseif ($request->sort_by=='high_to_low') {
                    $query->orderBy('price', 'desc');
                }
            } else {
                $query->orderBy('created_at', 'desc');
            }
            $items = $query->paginate(56);

            // Default Image
            $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
            if ($defaultItemImage)
                $defaultItemImage_path = asset($defaultItemImage->value);

            /*Notification Banner module*/
            $top_notification_banner_module = DB::table('top_banners')->where('category_id', $category->id)->get();
            if(count($top_notification_banner_module) == 0) {
                $top_notification_banner_module = [];
            }

            return view('pages.category', compact('category', 'items', 'defaultItemImage_path','top_notification_banner_module'))->with('url', $slug);
        } else {
            abort(404);
        }
    }

    public function secondCategory(Request $request, $slug, $slug2)
    {
        $category = Category::where('slug', '=', $slug2)->first();
        if (!empty($category)) {
            $query = Item::where('status', 1)->with('images.color')->where(function($query) use($category) {
                $query->where('default_parent_category', $category->id)
                ->orWhere('default_second_category', $category->id)
                ->orWhere('default_third_category', $category->id);
            });

            if (isset($request->sort_by)) {
                if ($request->sort_by=='low_to_high') {
                    $query->orderBy('price', 'asc');
                } elseif ($request->sort_by=='high_to_low') {
                    $query->orderBy('price', 'desc');
                }
            } else {
                $query->orderBy('created_at', 'desc');
            }
            $items = $query->paginate(56);

            // Default Image
            $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
            if ($defaultItemImage)
                $defaultItemImage_path = asset($defaultItemImage->value);

            /*Notification Banner module*/
            $top_notification_banner_module = DB::table('top_banners')->where('category_id', $category->id)->get();
            if(count($top_notification_banner_module) == 0) {
                $top_notification_banner_module = [];
            }

            return view('pages.category', compact('category', 'items', 'defaultItemImage_path','top_notification_banner_module'))->with('url', $slug.'/'.$slug2);
        } else {
            abort(404);
        }
    }

    public function newArrival(Request $request)
    {
        $today = Carbon::today();
        $query = Item::where('status', 1)->with('images.color')->where('created_at', '>', $today->subDays(60));

        if (isset($request->sort_by)) {
            if ($request->sort_by=='low_to_high') {
                $query->orderBy('price', 'asc');
            } elseif ($request->sort_by=='high_to_low') {
                $query->orderBy('price', 'desc');
            }
        } else {
            $query->orderBy('created_at', 'desc');
        }
        $items = $query->paginate(56);

        // Default Image
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        /*Notification Banner module*/
        $top_notification_banner_module = DB::table('top_banners')->where('page', '10')->get();
        if(count($top_notification_banner_module) == 0) {
            $top_notification_banner_module = [];
        }

        $category = (object) [
            'name' => 'New Arrival'
        ];

        return view('pages.category', compact('category', 'items', 'defaultItemImage_path','top_notification_banner_module'))->with('url', 'new_in');
    }

    public function bestSelling(Request $request)
    {
        $query = Item::where('items.status', 1);
        $query->with('images.color');
        $query->select(DB::raw('COUNT(order_items.total_qty) AS maxSale'), 'items.*');
        $query->join('order_items', 'items.id', '=', 'order_items.item_id');
        $query->join('orders', 'orders.id', '=', 'order_items.order_id');
        $query->groupBy('items.id');

        if (isset($request->sort_by)) {
            if ($request->sort_by=='low_to_high') {
                $query->orderBy('items.price', 'asc');
            } elseif ($request->sort_by=='high_to_low') {
                $query->orderBy('items.price', 'desc');
            }
        } else {
            $query->orderBy('items.created_at', 'desc');
        }
        $items = $query->paginate(56);

        // Default Image
        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        /*Notification Banner module*/
        $top_notification_banner_module = DB::table('top_banners')->where('page', '11')->get();
        if(count($top_notification_banner_module) == 0) {
            $top_notification_banner_module = [];
        }

        $category = (object) [
            'name' => 'Best Selling'
        ];

        return view('pages.category', compact('category', 'items', 'defaultItemImage_path','top_notification_banner_module'))->with('url', 'best_selling');
    }
}
