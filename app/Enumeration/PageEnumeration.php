<?php
namespace App\Enumeration;

class PageEnumeration {
    public static $ABOUT_US = 1;
    public static $CONTACT_US = 2;
    public static $PRIVACY_POLICY = 3;
    public static $RETURN_INFO = 4;
    public static $BILLING_SHIPPING_INFO = 5;
    public static $HOME = 6;
    public static $PARENT_CATEGORY = 7;
    public static $LARGE_QUANTITIES = 8;
    public static $REFUNDS = 9;
    public static $NEW_ARRIVAL = 10;
    public static $BEST_SELLER = 11;
    public static $TERMS_CONDITIONS = 12;
    public static $COOKIES_POLICY = 13;
    public static $SIZE_GUIDE = 14;
    public static $CHECK_ORDERS = 15;
    public static $RETURN_POLICY = 16;
    public static $PAYMENT_SHIPPING = 17;
    public static $TERMS_AND_CONDIOTIONS = 18;
    public static $HEADER_FOOTER_COLOR = 19;
}